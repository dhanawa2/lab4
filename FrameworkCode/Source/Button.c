/*----------------------------- Include Files -----------------------------*/
/* include header files for this service
*/

#include "Button.h"
#include "MorseElements.h"
#include "MorseDecode.h"
#include "LCDService.h"
/* include header files for hardware access
*/
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"
#define ALL_BITS (0xff<<2)
/* include header files for the framework
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "ES_DeferRecall.h"
#include "ES_ShortTimer.h"

/* include header files for the other modules in Lab3 that are referenced
*/
#include "LCD_Write.h"

/*----------------------------- Module Defines ----------------------------*/
// these times assume a 1.000mS/tick timing
#define ONE_SEC 1000
#define HALF_SEC (ONE_SEC/2)
#define TWO_SEC (ONE_SEC*2)
#define FIVE_SEC (ONE_SEC*5)

/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this service.They should be functions
   relevant to the behavior of this service
*/


/*---------------------------- Module Variables ---------------------------*/
// with the introduction of Gen2, we need a module level Priority variable
static uint8_t MyPriority;
static uint16_t LastButtonState;
static ButtonState_t CurrentState;
static uint16_t FirstDelta;
static uint16_t TimeOfLastRise;
static uint16_t TimeOfLastFall;
static uint16_t LengthOfDot;



// add a deferral queue for up to 3 pending deferrals +1 to allow for overhead
static ES_Event DeferralQueue[3+1];





//Morse Code Sample Pseudo Code Using the Gen2.x Event Framework
//Rev 14 10/21/15
//*************************************************************************************
//Pseudo-code for the Button module (a service that implements a state machine)
//Data private to the module: LastButtonState

//InitializeButtonDebounce
bool InitializeButtonDebounce(uint8_t Priority)
{
//Takes a priority number, returns True. 

//Initialize the MyPriority variable with the passed in parameter.
  MyPriority = Priority;
//Initialize the port line to monitor the button
  HWREG(GPIO_PORTB_BASE+GPIO_O_DEN)|=BIT4HI;
  HWREG(GPIO_PORTB_BASE+GPIO_O_DIR)&=BIT4LO;
//Sample the button port pin and use it to initialize LastButtonState
  LastButtonState= (HWREG(GPIO_PORTB_BASE+(GPIO_O_DATA + ALL_BITS)) &=(BIT4HI));
//Set CurrentState to be DEBOUNCING
  CurrentState = Debouncing;
//Start debounce timer (timer posts to ButtonDebounceSM)
  ES_Timer_InitTimer(DEBOUNCE_TIMER, 30);
//End of InitializeButton (return True)
  return true;
}

 bool PostButtonDebounceService( ES_Event ThisEvent )
{
  return ES_PostToService( MyPriority, ThisEvent);
}


//CheckButtonEvents
bool CheckButtonEvents(void)
{
//Takes no parameters, returns True if an event posted (11/04/11 jec)
//Local ReturnVal = False, CurrentButtonState
  bool ReturnVal = false;
  uint16_t CurrentButtonState;
//Set CurrentButtonState to state read from port pin
  CurrentButtonState=(HWREG(GPIO_PORTB_BASE+(GPIO_O_DATA + ALL_BITS))&=(BIT4HI));
//If the CurrentButtonState is different from the LastButtonState
//	Set ReturnVal = True
  if (CurrentButtonState!=LastButtonState)
  {
    ReturnVal=true;
//	If the CurrentButtonState is down
//		PostEvent ButtonDown to ButtonDebounce queue
    if(CurrentButtonState==0)
    {
      ES_Event ThisEvent;
      ThisEvent.EventType=ButtonDown;
      printf("Button down\n\r");
      PostButtonDebounceService(ThisEvent);
    }
//	Else
//		PostEvent ButtonUp to ButtonDebounce queue
    else
    {
      ES_Event ThisEvent;
      ThisEvent.EventType=ButtonUp;
          printf("Button up\n\r");
      PostButtonDebounceService(ThisEvent);
    }
//	Endif
//Endif
  }
//Set LastButtonState to the CurrentButtonState
  LastButtonState=CurrentButtonState;
//Return ReturnVal
  return ReturnVal;
//End of CheckButtonEvents
}

//RunButtonDebounceSM (implements a 2-state state machine for debouncing timing)
//The EventType field of ThisEvent will be one of: ButtonUp, ButtonDown, or ES_TIMEOUT

ES_Event RunButtonDebounceSM(ES_Event ThisEvent)
{
//If CurrentState is Debouncing
//	If EventType is ES_TIMEOUT & parameter is debounce timer number
//		Set CurrentState to Ready2Sample
  switch(CurrentState)
  {
    case Debouncing:
      if(ThisEvent.EventType==ES_TIMEOUT && ThisEvent.EventParam==DEBOUNCE_TIMER)
      {
        printf("in Debouncing\n\r");
        CurrentState=Ready2Sample;
      }
      break;
//Else if CurrentState is Ready2Sample
//	If EventType is ButtonUp
//		Start debounce timer
//		Set CurrentState to DEBOUNCING
//		Post DBButtonUp to MorseElements & DecodeMorse queues
//	End if
//	If EventType is ButtonDown
//		Start debounce timer
//		Set CurrentState to DEBOUNCING
//       Post DBButtonDown to MorseElements & DecodeMorse queues
// End if
//End Else
//Return ES_NO_EVENT
//End of RunButtonDebounceSM 
      
    case Ready2Sample:
      if(ThisEvent.EventType==ButtonUp)
      {
        ES_Timer_InitTimer(DEBOUNCE_TIMER,30);
        CurrentState=Debouncing;
        printf("DBButtonUp \n\r");
        ES_Event ButtonEvent;
        ButtonEvent.EventType=DBButtonUp;
        PostMorseElementsService(ButtonEvent);
        PostMorseDecodeService(ButtonEvent);
      }
      if(ThisEvent.EventType==ButtonDown)
      {
       ES_Timer_InitTimer(DEBOUNCE_TIMER,30);
       CurrentState=Debouncing;
       ES_Event ButtonEvent;
       printf("DBButtonDown \n\r");
       ButtonEvent.EventType=DBButtonDown;
       PostMorseElementsService(ButtonEvent);
       PostMorseDecodeService(ButtonEvent);
      }
      break;
    }
  ES_Event ReturnEvent;
  ReturnEvent.EventType=ES_NO_EVENT;
  return ReturnEvent;
  }
//  *************************************************************************************