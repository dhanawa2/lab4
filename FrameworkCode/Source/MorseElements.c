/*----------------------------- Include Files -----------------------------*/
/* include header files for this service
*/

#include "Button.h"
#include "MorseElements.h"
#include "MorseDecode.h"
#include "LCDService.h"
/* include header files for hardware access
*/
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"
#define ALL_BITS (0xff<<2)
/* include header files for the framework
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "ES_DeferRecall.h"
#include "ES_ShortTimer.h"

/* include header files for the other modules in Lab3 that are referenced
*/
#include "LCD_Write.h"

/*----------------------------- Module Defines ----------------------------*/
// these times assume a 1.000mS/tick timing
#define ONE_SEC 1000
#define HALF_SEC (ONE_SEC/2)
#define TWO_SEC (ONE_SEC*2)
#define FIVE_SEC (ONE_SEC*5)

/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this service.They should be functions
   relevant to the behavior of this service
*/


/*---------------------------- Module Variables ---------------------------*/
// with the introduction of Gen2, we need a module level Priority variable
static uint8_t MyPriority;
static uint16_t LastInputState;
static MorseElementsState_t CurrentState;
static uint16_t FirstDelta;
static uint16_t TimeOfLastRise;
static uint16_t TimeOfLastFall;
static uint16_t LengthOfDot;



// add a deferral queue for up to 3 pending deferrals +1 to allow for overhead
static ES_Event DeferralQueue[3+1];



void CharacterizeSpace(void);
void CharacterizePulse(void);




//Morse Code Sample Pseudo Code Using the Gen2.x Event Framework
//Rev 14 10/21/15
//*************************************************************************************
//Pseudo-code for the Morse Elements module (a service that implements a state machine)
//Data private to the module: MyPriority, CurrentState, TimeOfLastRise, TimeOfLastFall, LengthOfDot, FirstDelta, LastInputState


//InitializeMorseElements
bool InitializeMorseElements( uint8_t Priority)
{
//Takes a priority number, returns True. 
 printf("initializing\n\r");
//Initialize the MyPriority variable with the passed in parameter.
  MyPriority = Priority;
//Initialize the port line to receive Morse code
  HWREG(GPIO_PORTB_BASE+GPIO_O_DEN)|=BIT3HI;
  HWREG(GPIO_PORTB_BASE+GPIO_O_DIR)&=BIT3LO;
//Sample port line and use it to initialize the LastInputState variable
  LastInputState= (HWREG(GPIO_PORTB_BASE+(GPIO_O_DATA + ALL_BITS)) &=(BIT3HI));
//Set CurrentState to be InitMorseElements
  CurrentState=InitMorseElements;
//Set FirstDelta to 0
  FirstDelta=0;
//Post Event ES_Init to MorseElements queue (this service)
   ES_Event ThisEvent;
  ThisEvent.EventType = ES_INIT;
  if (ES_PostToService( MyPriority, ThisEvent) == true)
  {  
    return true;
  }else
  {
      return false;
  }
  printf("Done Initializing\n\r");
/*IF THIS DOES NOT WORK THEN USE PostMorseElementsService function instead of just ES_PostToService*/
 
  
//End of InitializeMorseElements

}



 bool PostMorseElementsService( ES_Event ThisEvent )
{
  return ES_PostToService( MyPriority, ThisEvent);
}


//CheckMorseEvents 
bool CheckMorseEvents()
{
  //printf("Checking\n\r");
//Takes no parameters, returns True if an event was posted (11/4/11 jec)
//local RetrunVal = False, CurrentInputState
  bool ReturnVal = false;
  uint16_t CurrentInputState;
  ES_Event ThisEvent;
//Get the CurrentInputState from the input line
  CurrentInputState=(HWREG(GPIO_PORTB_BASE+(GPIO_O_DATA + ALL_BITS))&=(BIT3HI));
//If the state of the Morse input line has changed
  if(CurrentInputState!=LastInputState)
    {
      //printf("Not equal\n\r");
//		If the current state of the input line is high
//			PostEvent RisingEdge with parameter of the Current Time
      if(CurrentInputState)
        {
          ES_Event ThisEvent;
          ThisEvent.EventType= RisingEdge;
          ThisEvent.EventParam = ES_Timer_GetTime();
          PostMorseElementsService(ThisEvent);
        }
//		Else (current input state is low)
//			PostEvent FallingEdge with parameter of the Current Time
      else 
        {
          ES_Event ThisEvent;
          ThisEvent.EventType= FallingEdge;
          ThisEvent.EventParam = ES_Timer_GetTime();
          PostMorseElementsService(ThisEvent);
        }        
//		Endif
//		Set ReturnVal = True
     ReturnVal=true;
     
//Endif
  }
//Set LastInputState to CurrentInputState
  LastInputState=CurrentInputState;
//Return ReturnVal
  return ReturnVal;
//End of CheckMorseEvents
}


/*ES_Event RunMorseElementsSM(ES_Event ThisEvent)
{
  ES_Event ReturnEvent;
  ReturnEvent.EventType = ES_NO_EVENT;
  if(ThisEvent.EventType==RisingEdge)
  {
    printf("R\n\r");
    
  }
    if(ThisEvent.EventType==FallingEdge)
  {
    printf("F\n\r");
  }
  return ReturnEvent;
}*/


//RunMorseElementsSM (implements the state machine for Morse Elements)
ES_Event RunMorseElementsSM(ES_Event ThisEvent)
{
//The EventType field of ThisEvent will be one of: ES_Init, RisingEdge, FallingEdge, CalibrationCompleted, EOCDetected, DBButtonDown.  The parameter field of the ThisEvent will be the time that the event occurred.
//Returns ES_NO_EVENT 

//Local Variables: NextState
  
//Set NextState to CurrentState
  MorseElementsState_t NextState=CurrentState;
//Based on the state of the CurrentState variable choose one of the following blocks of code:
  
  switch (CurrentState)
    {
//		CurrentState is InitMorseElements
//			If ThisEvent is ES_Init
//				Set NextState to CalWaitForRise
//			Endif 
//		End InitMorseElements block
      case InitMorseElements:
        if(ThisEvent.EventType==ES_INIT)
          {
            //printf("Init Case \n\r");
            NextState=CalWaitForRise;
          }
      break;
         

//		CurrentState is CalWaitForRise
//			If ThisEvent is RisingEdge
//				Set TimeOfLastRise to Time from event parameter
//				Set NextState to CalWaitForFall
//			Endif 
//			If ThisEvent is CalibrationComplete
//				Set NextState to EOC_WaitRise
//			Endif 
//		End CalWaitForRise block
      case CalWaitForRise:
        if(ThisEvent.EventType==RisingEdge)
        {
          TimeOfLastRise = ThisEvent.EventParam;
          //printf("Rising Edge Case \n\r");
          NextState=CalWaitForFall;
        }
        if(ThisEvent.EventType==CalibrationCompleted)
        {
          NextState=EOC_WaitRise;
          //printf("Length of Dot is %u\n\r",LengthOfDot);
        }
      break;

//		CurrentState is CalWaitForFall
//			If ThisEvent is FallingEdge
//				Set TimeOfLastFall to Time from event parameter
//				Set NextState to CalWaitForRise
//				Call TestCalibration function
//				EndIf
//		End CalWaitForFall block
      case CalWaitForFall:
        if(ThisEvent.EventType==FallingEdge)
        {
          TimeOfLastFall = ThisEvent.EventParam;
          //printf("Falling Edge Case \n\r");
          NextState=CalWaitForRise;
          TestCalibration();
        }
        break;

//		CurrentState is EOC_WaitRise
//			If ThisEvent is RisingEdge
//				Set TimeOfLastRise to Time from event parameter
//				Set NextState to EOC_WaitFall
//				Call CharacterizeSpace function
//			Endif 
//			If ThisEvent is DBButtonDown
//				Set NextState to CalWaitForRise
//				Set FirstDelta to 0
//			Endif 
//		End EOC_WaitRise block
      case EOC_WaitRise:
        if(ThisEvent.EventType==RisingEdge)
        {
          TimeOfLastRise = ThisEvent.EventParam;
          NextState=EOC_WaitFall;
          CharacterizeSpace();
        }  
        if(ThisEvent.EventType==DBButtonDown)
        {
          printf("Button Down\n\r");
          NextState=CalWaitForRise;
          FirstDelta=0;
        }
        break;
//		CurrentState is EOC_WaitFall
//			If ThisEvent is FallingEdge
//				Set TimeOfLastFall to Time from event parameter
//				Set NextState to EOC_WaitRise
//				EndIf
//			If ThisEvent is DBButtonDown
//				Set NextState to CalWaitForRise
//				Set FirstDelta to 0
//			Endif 
//			If ThisEvent is EOCDetected
//				Set NextState to DecodeWaitFall
//			Endif 
//		End EOC_WaitFall block
      case EOC_WaitFall:
        if(ThisEvent.EventType==FallingEdge)
        {
          TimeOfLastFall = ThisEvent.EventParam;
          NextState=EOC_WaitRise;
        }  
        if(ThisEvent.EventType==EOCDetected)
        {
          NextState=DecodeWaitFall;
        }
        break; 

//		CurrentState is DecodeWaitRise
//			If ThisEvent is RisingEdge
//				Set TimeOfLastRise to Time from event parameter
//				Set NextState to DecodeWaitFall
//				Call CharacterizeSpace function
//			Endif 
//			If ThisEvent is DBButtonDown
//				Set NextState to CalWaitForRise
//				Set FirstDelta to 0
//			Endif 
//		End DecodeWaitRise block
      case DecodeWaitRise:
        if(ThisEvent.EventType==RisingEdge)
        {
          TimeOfLastRise = ThisEvent.EventParam;
          NextState=DecodeWaitFall;
          CharacterizeSpace();
        }  
        if(ThisEvent.EventType==DBButtonDown)
        {
          printf("Button Down\n\r");
          NextState=CalWaitForRise;
          FirstDelta=0;
        }
        break; 

//		CurrentState is DecodeWaitFall
//			If ThisEvent is FallingEdge
//				Set TimeOfLastFall to Time from event parameter
//				Set NextState to DecodeWaitRise
//				Call CharacterizePulse function
//			Endif 
//			If ThisEvent is DBButtonDown
//				Set NextState to CalWaitForRise
//				Set FirstDelta to 0
//			Endif 
//		End DecodeWaitFall block
      case DecodeWaitFall:
        if(ThisEvent.EventType==FallingEdge)
        {
          //printf("Decode Wait Fall\n\r");
          TimeOfLastFall = ThisEvent.EventParam;
          NextState=DecodeWaitRise;
          CharacterizePulse();
        }  
        if(ThisEvent.EventType==DBButtonDown)
        {
          printf("Button Down\n\r");
          NextState=CalWaitForRise;
          FirstDelta=0;
        }
        break;
 }
//Set CurrentState to NextState
  CurrentState=NextState;

//Return ES_NO_EVENT
  ES_Event ReturnEvent;
  ReturnEvent.EventType = ES_NO_EVENT;
  return ReturnEvent;
//End of RunMorseElementsSM 
}




//TestCalibration
//Takes no parameters, returns nothing. 

void TestCalibration(void)
  {
    printf("Calibrating\n\r");
//Local variable SecondDelta
    uint16_t SecondDelta;
//If calibration is just starting (FirstDelta is 0)
//		Set FirstDelta to most recent pulse width
    if (FirstDelta==0)
      {
        FirstDelta=TimeOfLastFall-TimeOfLastRise;
        //printf("First Delta is %u\n\r",FirstDelta);
      }
//Else
//		Set SecondDelta to most recent pulse width
    else
      {
        SecondDelta=TimeOfLastFall-TimeOfLastRise;
        //printf("Second Delta is %u\n\r",SecondDelta);
//		If (100.0 * FirstDelta / SecondDelta) less than or equal to 33.33
//			Save FirstDelta as LengthOfDot
//			PostEvent CalCompleted to MorseElementsSM
        if((100.0*(FirstDelta/SecondDelta))<=33.33)
          {
            LengthOfDot=FirstDelta;
            printf("Length of Dot is %u\n\r",LengthOfDot);
            ES_Event CalEvent;
            CalEvent.EventType=CalibrationCompleted;
            PostMorseElementsService(CalEvent);
          }
//		ElseIf (100.0 * FirstDelta / Second Delta) greater than 300.0
//			Save SecondDelta as LengthOfDot
//			PostEvent CalCompleted to MorseElementsSM
        else if ((100.0*(FirstDelta/SecondDelta))>=300.00)
        {
          LengthOfDot=SecondDelta;
          printf("Length of Dot is %u\n\r",LengthOfDot);
          ES_Event CalEvent;
          CalEvent.EventType=CalibrationCompleted;
          PostMorseElementsService(CalEvent);
        }
//		Else (prepare for next pulse)
//			SetFirstDelta to SecondDelta
        else
        {
          FirstDelta=SecondDelta;
        }         
//		EndIf        
//EndIf
      }

//Return
  printf("End of Calibrating\n\r");    
  return;      
//End of TestCalibration
  }
//CharacterizeSpace
//Takes no parameters, returns nothing.
//Posts one of EOCDetected Event, EOWDetected Event, BadSpace Event as appropriate
//on good dot-space, does nothing
void CharacterizeSpace(void)
{
//Local variable LastInterval, Event2Post
  uint16_t LastInterval;
  ES_Event Event2Post;
  
//Calculate LastInterval as TimeOfLastRise � TimeOfLastFall
  LastInterval=TimeOfLastRise-TimeOfLastFall;
//If LastInterval not OK for a Dot Space
//	If LastInterval OK for a Character Space
//		PostEvent EOCDetected Event to Decode Morse Service & Morse Elements Service
//	Else
//		If LastInterval OK for Word Space
//			PostEvent EOWDetected Event to Decode Morse Service
//		Else
//			PostEvent BadSpace Event to Decode Morse Service
//		EndIf
//	Endif
//EndIf
  //printf("Last Interval is %u\n\r" , LastInterval);
  if((LastInterval>(LengthOfDot+8))||(LastInterval<(LengthOfDot-8)))
  {
    if((LastInterval<(LengthOfDot*3+24))&&(LastInterval>(LengthOfDot*3-24)))
    {
      //printf(" ");
      Event2Post.EventType=EOCDetected;
      PostMorseElementsService(Event2Post);
      PostMorseDecodeService(Event2Post);
    }
    else
    {
      if((LastInterval<(LengthOfDot*7+60))&&(LastInterval>(LengthOfDot*7-60)))
        {
          //printf("    ");
          Event2Post.EventType=EOWDetected;
          //PostMorseElementsService(Event2Post);
          PostMorseDecodeService(Event2Post);
        }
      else
        {
          Event2Post.EventType=BadSpaceEvent;
          //PostMorseElementsService(Event2Post);
          PostMorseDecodeService(Event2Post);
        }
    }
  }
//Return
  return;
//End of CharacterizeSpace
}
//CharacterizePulse
//Takes no parameters, returns nothing.
//Posts one of DotDetectedEvent, DashDetectedEvent, BadPulseEvent,
void CharacterizePulse(void)
{
  //printf("CharacterizePulse \n\r");
//Local variable LastPulseWidth, Event2Post
  uint16_t LastPulseWidth;
  ES_Event Event2Post;
  
//Calculate LastPulseWidth as TimeOfLastFall - TimeOfLastRise
//If LastPulseWidth OK for a dot
//		PostEvent DotDetected Event to Decode Morse Service
//Else
//		If LastPulseWidth OK for dash
//			PostEvent DashDetected Event to Decode Morse Service
//		Else
//			PostEvent BadPulse Event to Decode Morse Service
//		EndIf
  //printf("Time of Last Rise %u\n\r", TimeOfLastRise); 
  //printf("Time of Last Fall %u\n\r", TimeOfLastFall);
  LastPulseWidth=TimeOfLastFall-TimeOfLastRise;
  //printf("Last Pulse Width %u", LastPulseWidth);
  if((LastPulseWidth<(LengthOfDot+8))&&(LastPulseWidth>(LengthOfDot-8)))
  {
    //printf(".");
    Event2Post.EventType=DotDetectedEvent;
    //PostMorseElementsService(Event2Post);
    PostMorseDecodeService(Event2Post);
  }  
  else
  {
    if((LastPulseWidth<(LengthOfDot*3+24))&&(LastPulseWidth>(LengthOfDot*3-24)))
    {
      //printf("-");
      Event2Post.EventType=DashDetectedEvent;
      //PostMorseElementsService(Event2Post);
      PostMorseDecodeService(Event2Post);
    }
    else
    {
      Event2Post.EventType=BadPulseEvent;
      //PostMorseElementsService(Event2Post);
      PostMorseDecodeService(Event2Post);
    }
  }
//EndIf
//Return 
  return;
//End of CharacterizePulse
}

//*************************************************************************************