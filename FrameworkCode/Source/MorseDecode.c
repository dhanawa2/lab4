/*----------------------------- Include Files -----------------------------*/
/* include header files for this service
*/

#include "Button.h"
#include "MorseElements.h"
#include "MorseDecode.h"
#include "LCDService.h"
/* include header files for hardware access
*/
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"
#define ALL_BITS (0xff<<2)
/* include header files for the framework
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "ES_DeferRecall.h"
#include "ES_ShortTimer.h"

/* include header files for the other modules in Lab3 that are referenced
*/
#include "LCD_Write.h"
#include "string.h"
#include "stdio.h"

/*----------------------------- Module Defines ----------------------------*/
// these times assume a 1.000mS/tick timing
#define ONE_SEC 1000
#define HALF_SEC (ONE_SEC/2)
#define TWO_SEC (ONE_SEC*2)
#define FIVE_SEC (ONE_SEC*5)

/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this service.They should be functions
   relevant to the behavior of this service
*/


/*---------------------------- Module Variables ---------------------------*/
// with the introduction of Gen2, we need a module level Priority variable
static uint8_t MyPriority;
char legalChars[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890?.,:'-/()\"= ";
char morseCode[][8] ={ ".-","-...","-.-.","-..",".","..-.","--.",
                      "....","..",".---","-.-",".-..","--","-.","---",
                      ".--.","--.-",".-.","...","-","..-","...-",
                      ".--","-..-","-.--","--..",".----","..---",
                      "...--","....-",".....","-....","--...","---..",
                      "----.","-----","..--..",".-.-.-","--..--",
                      "---...",".----.","-....-","-..-.","-.--.-",
                      "-.--.-",".-..-.","-...-"
                     };
char MorseString[12];
int counter=0;



// add a deferral queue for up to 3 pending deferrals +1 to allow for overhead
static ES_Event DeferralQueue[3+1];





//Morse Code Sample Pseudo Code Using the Gen2.x Event Framework
//Rev 14 10/21/15
//*************************************************************************************
//Pseudo-code for the Decode Morse module (a simple service)
//Data private to the module: MorseString, the arrays LegalChars and MorseCode

//InitMorseDecode
//Takes a priority number, returns True. 
bool InitializeMorseDecode(uint8_t Priority)
{
//Initialize the MyPriority variable with the passed in parameter.
//Clear (empty) the MorseString variable
//End of InitMorseDecode
  //printf("Init Morse decode \n\r");
  MyPriority = Priority;
  memset(MorseString,'\0',sizeof(MorseString));
  counter=0;
  return true;
}

bool PostMorseDecodeService( ES_Event ThisEvent )
{
    return ES_PostToService( MyPriority, ThisEvent);
}
//RunMorseDecode (implements the service for Morse Decode)
//The EventType field of ThisEvent will be one of: DotDetectedEvent, DashDetectedEvent, EOCDetected, EOWDetected, ButtonDown.
//Returns ES_NO_EVENT if No Error detected, ES_ERROR otherwise
//local var ReturnValue initialized to ES_NO_EVENT
ES_Event RunMorseDecode(ES_Event ThisEvent)
{
  //printf("Run Morse decode \n\r");
  ES_Event ReturnValue;
  ReturnValue.EventType=ES_NO_EVENT;
//Based on the state of the ThisEvent variable choose one of the following blocks of code:
//	If ThisEvent is DotDetected Event
//		If there is room for another Morse element in the internal representation
//			Add a Dot to the internal representation
//		Else
//			Set ReturnValue to ES_ERROR with param set to indicate this location
//	End if ThisEvent is DotDetected Event

  if(ThisEvent.EventType==DotDetectedEvent)
  {
    //printf("Dot \n\r");
    if(counter<12)
    {
      MorseString[counter]='.';
      counter++;
    }
    else
    {
      ReturnValue.EventType=ES_ERROR;
      ReturnValue.EventParam=counter;
    }
  }
//	If ThisEvent is DashDetected Event
//		If there is room for another Morse element in the internal representation
//			Add a Dash to the internal representation
//		Else
//			Set ReturnValue to ES_ERROR with param set to indicate this location
//	End if ThisEvent is DashDetected Event
  if(ThisEvent.EventType==DashDetectedEvent)
  {
    //printf("Dash \n\r");
    if(counter<12)
    {
      MorseString[counter]='-';
      counter++;
    }
    else
    {
      ReturnValue.EventType=ES_ERROR;
      ReturnValue.EventParam=counter;
    }
  }

//	If ThisEvent is EOCDetected Event
//		call DecodeMorse to try and match current MorseString
//		Print to LCD the decoded character
//		Clear (empty) the MorseString variable
//	End if is EOCDetected Event

  if(ThisEvent.EventType==EOCDetected)
  {
    //printf("End of Char \n\r");
    char x = DecodeMorseString();
    ES_Event LCDEvent;
    LCDEvent.EventType=ES_LCD_PUTCHAR; 
    LCDEvent.EventParam=x;
    PostLCDService(LCDEvent);
    memset(MorseString,'\0',sizeof(MorseString));
    counter=0;
  }

//	If ThisEvent is EOWDetected Event
//		call DecodeMorse to try and match current MorseString
//		Print to LCD the decoded character
//		Print to the LCD a space
//		Clear (empty) the MorseString variable
//	End if ThisEvent is EOWDetected Event
  if(ThisEvent.EventType==EOWDetected)
  {
    char x = DecodeMorseString();
    ES_Event LCDEvent;
    LCDEvent.EventType=ES_LCD_PUTCHAR; 
    LCDEvent.EventParam= x;
    PostLCDService(LCDEvent);
    LCDEvent.EventType=ES_LCD_PUTCHAR; 
    LCDEvent.EventParam= ' ';
    PostLCDService(LCDEvent);
    memset(MorseString,'\0',sizeof(MorseString));
    counter=0;
  }
//	If ThisEvent is BadSpace Event  (Added 10/19/15 JEC)
//		Clear (empty) the MorseString variable
//	End if ThisEvent is BadSpace Event
  if(ThisEvent.EventType==BadSpaceEvent)
  {
    memset(MorseString,'\0',sizeof(MorseString));
    counter=0;
  }
//	If ThisEvent is BadPulse Event  (Added 10/19/15 JEC)
//		Clear (empty) the MorseString variable
//	End if ThisEvent is BadPulse Event
  if(ThisEvent.EventType==BadPulseEvent)
  {
    memset(MorseString,'\0',sizeof(MorseString));
    counter=0;
  }
//	If ThisEvent is DBButtonDown Event  (Added 11/3/11 JEC)
//		Clear (empty) the MorseString variable
//	End if ThisEvent is ButtonDown Event
  if(ThisEvent.EventType==DBButtonDown)
  {
    memset(MorseString,'\0',sizeof(MorseString));
    counter=0;
  }
//return ReturnValue
  return ReturnValue;
//End MorseDecode

}

//DecodeMorseString
//Takes no parameters, returns either a character or a '~' indicating failure
char DecodeMorseString(void)
{ 
//		For every entry in the array MorseCode
//			If MorseString is the same as current position in MorseCode 
//				return contents of current position in LegalChars
//			EndIf
//		EndFor
  for(int i =0; i<sizeof(morseCode);i++)
  {
    if(strcmp(MorseString,morseCode[i])==0)
    {
      return legalChars[i];
    }
  }
//return '~', since we didn't find a matching string in MorseCode
  return'~';
//End of DecodeMorseString
}

//*************************************************************************************