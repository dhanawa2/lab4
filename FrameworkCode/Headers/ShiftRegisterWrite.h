// create your own header file comment block
// and protect against multiple inclusions

#ifndef SHIFTREGISTERWRITE_H
#define SHIFT REGISTERWRITE_H

#include <stdint.h>
#include <stdbool.h>

void SR_Init(void);
uint8_t SR_GetCurrentRegister(void);
void SR_Write(uint8_t NewValue);
#endif
