#ifndef Button_H
#define Button_H

// Event Definitions
#include "ES_Configure.h" /* gets us event definitions */
#include "ES_Types.h"     /* gets bool type for returns */
#include "ES_Events.h"
// typedefs for the states
// State definitions for use with the query function
typedef enum { Debouncing, Ready2Sample } ButtonState_t ;

// Public Function Prototypes

bool InitializeButtonDebounce( uint8_t Priority);
 bool PostButtonDebounceService( ES_Event ThisEvent );
bool CheckButtonEvents(void);
ES_Event RunButtonDebounceSM(ES_Event ThisEvent);
#endif /* Button_H */