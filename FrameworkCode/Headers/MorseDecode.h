#ifndef MorseDecode_H
#define MorseDecode_H

// Event Definitions
#include "ES_Configure.h" /* gets us event definitions */
#include "ES_Types.h"     /* gets bool type for returns */
#include "ES_Events.h"
// typedefs for the states
// State definitions for use with the query function

// Public Function Prototypes

bool InitializeMorseDecode( uint8_t Priority);
 bool PostMorseDecodeService( ES_Event ThisEvent );
ES_Event RunMorseDecode(ES_Event ThisEvent);
char DecodeMorseString();
#endif /* Button_H */